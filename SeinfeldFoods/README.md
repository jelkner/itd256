# Seinfeld Foods

Data and database script files for setting up Seinfeld Foods database.


To setup the seinfeld\_foods database, do the following:

1. Run:
   > $ createdb seinfeld\_foods

   to create the database.

2. Run:
   > $ make

   from within the 'SeinfeldFoods' directory to populate the database.
