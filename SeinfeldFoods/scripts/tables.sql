DROP TABLE IF EXISTS episodes CASCADE;
DROP TABLE IF EXISTS foods CASCADE;
DROP TABLE IF EXISTS foods_episodes CASCADE;
DROP TABLE IF EXISTS food_types CASCADE;

CREATE TABLE episodes (
    id       INT PRIMARY KEY,
    season   INT,
    name     TEXT
);
CREATE TABLE foods (
    id       INT PRIMARY KEY,
    type_id  INT,
    name     TEXT
);
CREATE TABLE foods_episodes(
    food_id    INT,
    episode_id INT
);
CREATE TABLE food_types (
    id       INT PRIMARY KEY,
    name     TEXT
);
