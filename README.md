# ITD 256: Advanced Database Management

Data and database resources and notes for VCCS ITD 256 course.

## Notes Directory 

* [Beginning Postgres](BeginningPostgres)


## Links

* [FoxTrot Joins Example Database Scripts](https://codeberg.org/GCTAA/FoxTrot)
* [My Curious Moon git repo](https://gitlab.com/jelkner/curious-moon)
* [Postgres Cheat Sheet](https://postgrescheatsheet.com/)

## Connecting to Remote DB with psql
```
psql -h <host> -p <port> -d <database> -U <user>
```
