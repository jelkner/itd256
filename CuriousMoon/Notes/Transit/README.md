# Notes from Second Chapter: Transit 


## Vocab List

* zip file (page 12)
* box (page 13)
* Postgres.app (page 13)
* Homebrew (page 13) 
* Docker (page 13)
* .NET (page 13)
* Navicat (page 13)
* SQLPro (page 13)
* Postico (page 13)
* PG Admin IV (page 13)
* psql (page 14)
* log (page 14)
* symlink (page 15)
* Hacker News (page 15)


## Postgres Shell Scripts 

* createdb (page 14)
* dropdb (page 14)


## psql Commands

* help


## Beginning Postgres Tutorials

On page 20, Dee mentions the
[SQL Tutorial](https://www.postgresql.org/docs/current/index.html) on the
[Postgres website](https://www.postgresql.org/).

Since she took time to look at this in this stage of her process, and since
she started out knowing more SQL than we do, we should take *more time* to
look at it now.


## Reading Spreadsheet Data into Postgres

Our story provides us with a really nice connection between our
[ITE 140](https://gitlab.com/jelkner/ite140) investigations and our entrée into
databases.

Here are two links with more information about reading
[cvs](https://en.wikipedia.org/wiki/Comma-separated_values) files into
postgres:

* [Import Excel Data into PostgreSQL 9.3](https://stackoverflow.com/questions/20039856/import-excel-data-into-postgresql-9-3)
* [How to Connect Excel to PostgreSQL? | Easy Ways to Load Data](https://hevodata.com/learn/excel-to-postgresql/)
