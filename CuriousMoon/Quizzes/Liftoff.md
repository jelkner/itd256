# Liftoff Quiz

1. Who is Dee, and what is the situation she find herself in as presented in
   the *Liftoff* chapter?

2. Describe to the best of your understanding what caused the DBA at Red:4 to
   be escorted out of the building.
