# Regular Expressions

[Regular expressions](https://en.wikipedia.org/wiki/Regular_expression) are
introduced in our *Curious Moon* book in the *Liftoff* chapter.

I'll put a list of useful resources for diving a bit deeper into them here.

## Links

* [RegexOne](https://regexone.com/): Learn Regular Expressions with simple,
  interactive exercises.
* [Regular Expresions 101](https://regex101.com/)
* [RegEx Explainer](https://regexper.com/)
