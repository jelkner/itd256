# What's on this System?

OK, so I can now ssh into my shell account on our database server, and
run
```
  $ psql
```
which connects my Postgres role with the same name as my shell user to a
database with the same name.  So now what do I do?  What can I learn about
the databases that are on this system?

## Using the Cheat Sheet

Using the [Postgres Cheat Sheet](https://postgrescheatsheet.com), clicking
on the *Databases* link, I learn that to see all the databases on the system,
I should run:
```
  jelkner=# \l
```
Since I added the following lines at the end of my ``.bashrc`` file:
```
EDITOR=vim
PAGER=most

export EDITOR PAGER
```
I get a nice display of the 19 databases currently on the system from ``most``.

