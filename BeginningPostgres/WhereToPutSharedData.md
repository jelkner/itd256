# Where to Put Shared Data

The [Filesystem Hierachy Standard](https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard)
describes conventions for the layout of the UNIX file system.

A quick search of "where to put shared data on linux file system" lead me to
[/usr/share : Architecture-independent data](https://refspecs.linuxfoundation.org/FHS_3.0/fhs/ch04s11.html), so unless Aaron objects, we will put our shared
data files for *A Curious Moon* in:
```
/usr/local/share/data/curious_moon
```

Let's add files to this sub-directory as we need them, so to start with, only
``master_plan.csv`` is there.
