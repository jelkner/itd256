# What Is PostgreSQL?

* [PostgreSQL](https://www.postgresql.org/) is an enterprise level free
  software
  [relational database](https://en.wikipedia.org/wiki/Relational_database).
* Originally developed as a successor to
  [Ingres](https://en.wikipedia.org/wiki/Ingres_\(database\)) at the
  University of California Berkley in 1986, giving it its name.
