# Beginning Postgres

To quick start our work with Postgres, we will complete
[PostgreSQL Tutorial for Beginners: Learn Basic PSQL in 3 Days](https://www.guru99.com/postgresql-tutorial.html), by Richard Peterson.

This contains my notes from that tutorial.

## Lessons Studies

* [What is PostgreSQL? Introduction, Advantages & Disadvantages](https://www.guru99.com/introduction-postgresql.html)
* [PostgreSQL Data Types: Byte, Numeric, Character, Binary](https://www.guru99.com/postgresql-data-types.html)
* [PostgreSQL/Postgres Create Database: How to Create Example](https://www.guru99.com/postgresql-create-database.html)

## Lesson Notes

* [What is PostgreSQL?](WhatIsPostgres.md)
* [Postgres Data Types](DataTypes.md)
* [Create / Drop Database](CreateDropDatabase.md)
* [Create / Drop User](CreateDropUser.md)


## Classroom DB Server Setup 

* [Getting Started](GettingStarted.md)
* [What's on This System?](WhatsOnThisSystem.md)
* [Where to Put Shared Data](WhereToPutSharedData.md)
