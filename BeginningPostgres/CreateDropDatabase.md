# Create Database in PostgreSQL

At the psql prompt, the command to create a database is:
```
# CREATE DATABASE [database name];
```

To see all the databases available on the system:
```
# \l
```

To connect to a database:
```
# \c [database name]
```

# Drop a Database in PostgreSQL

To drop a database:
```
# DROP DATABASE IF EXISTS [database name];
```

`IF EXISTS` is optional, but if it is used with a non-existant database name
a warning will be given instead of an error.
