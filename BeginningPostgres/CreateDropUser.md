# Create User in PostgreSQL

At the psql prompt, the syntax to create a user is:
```
CREATE USER name WITH option(s)

where the option(s) can be:

| SUPERUSER | NOSUPERUSER 
| CREATEROLE | NOCREATEROLE 
| CREATEDB | NOCREATEDB 

| INHERIT | NOINHERIT 

| LOGIN | NOLOGIN 

| REPLICATION | NOREPLICATION 

| BYPASSRLS  | NOBYPASSRLS 

| CONNECTION LIMIT 

| [ENCRYPTED] PASSWORD 'password.' 

| VALID UNTIL 'timestamp1 

| IN ROLE role_name [, ...] 

| IN GROUP role_name [, ...]

| ROLE role_name [, ...]

| ADMIN role_name [, ...]

| USER role_name [, ...]

| SYSID uid
```

## Examples

To create user `mstonebraker`:
```
# CREATE USER mstonebraker;
```

To create a superuser:
```
# CREATE USER mstonebraker WITH SUPERUSER;
```

To create a superuser valid untill 3rd Apri 2025 11:50:38 IST:
```
CREATE USER mstonebraker WITH
	LOGIN
	SUPERUSER
	CREATEDB
	CREATEROLE
	INHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	VALID UNTIL '2025-04-03T11:50:38+05:30' 
	PASSWORD '123456';
```
