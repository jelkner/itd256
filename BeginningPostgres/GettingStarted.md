# Getting Started on Our Classroom Database Server

I used [ssh](https://en.wikipedia.org/wiki/Secure_Shell) to login to our
classroom database server from my laptop. At the
[bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) prompt, I tried
starting ``psql`` with this result:
```
  $ psql
  psql: error: FATAL: role "jelkner" does not exist
```
What does this mean, and what can I do about it?

## What are roles in PostgreSQL?

### Creating a New User

To understand roles in Postgres, read the *What are roles?* section of
[Managing roles and role attributes in PostgreSQL](https://www.prisma.io/dataguide/postgresql/authentication-and-authorization/role-management)
in which you will find that the ``roles`` have the function of both ``users``
and ``groups`` in a Unix environment.

So I need to create a *role* jelkner.  How can I do that?  The ``createuser``
shell script is the easiest way to do this.  It can also be accomplished from
within ``psql`` using the
[CREATE ROLE](https://www.postgresql.org/docs/current/sql-createrole.html)
command.

For either of these, I can't do them as the ``jelkner`` user, so I will need
to become a user that can.  Fortunately, on our classroom database server
our system administrator has given us all the ability to become the
``postgres`` user by running:
```
  $ sudo su - postgres
```
I then ran:
```
  $ createuser --interactive jelkner 
  Shall the new role be a superuser (y/n) y
```
Responding with **y** to this first query ended the session.  Had I answered
**n**, I would have been prompted with a series of other questions, including:
```
  Shall the new role be allowed to create databases? (y/n)
  Shall the new role be allowed to create more new roles? (y/n)
```
Why are these questions not asked if you answer yes to the making the new
user a superuser?


### Creating a New Database 

With my new superuser ``jelkner`` created, I typed ``exit`` from the
``postgres`` user and returned to being my old ``jelkner`` self.  I then had
new problem when I tried to start ``psql``:
``` 
  $ psql
  psql: error: FATAL: database "jelkner" does not exist
``` 
Why this error and what can be done about it?

The simplest solution is to use my new superuser powers to run the
``createdb`` shell script:
```
  $ createdb jelkner
  $ psql
  psql (13.9 (Debian 13.9-0+debian11u1))
  Type "help" for help.

  jelkner=# 
```
I'm in!  What would be an alternative to creating a ``jelkner`` database?

I'll leave to you all to take it from here.
