# PostgreSQL Notes and Resource Links

## Examples

To execute a command remotely:
```
$ PGPASSWORD=[password] psql -h [url] -U [user] -d [db] -c "sql;"
```

To run a script on your local machine:
```
$ PGPASSWORD=[password] psql -h [url] -U [user] -d [db] -c [path_to_script]; 
```

## Links

* [PostgreSQL: execute command remotely](https://naysan.ca/2020/07/19/postgresql-execute-command-remotely/)
