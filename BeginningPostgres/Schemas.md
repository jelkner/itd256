# Schemas

## Two ways to list the available schema:
```
SELECT schema_name
FROM information_schema.schemata;
```

or
```
SELECT nspname
FROM pq_catalog.pg_namespace;
```
