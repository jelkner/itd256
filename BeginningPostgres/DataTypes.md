# PostgreSQL Data Types

PostgreSQL supports the following data types:


## Character Datatypes

| Name       | Description                             |
|------------|-----------------------------------------|
| varchar(n) | Variable length characters with limit n |
| Char(n)    | Fix length characters padded with blank |
| Text       | Unlimited length characters             |


## Numeric Datatypes

Postgres support two distinct numberic types, integers and floats, in a range
of sizes.

| Name     | Size     | Range                                                 |
|----------|----------|-------------------------------------------------------|
| smallint | 2 bytes  | -32768 to 32767                                       |
| integer  | 4 bytes  | -2147483648 to 2147483647                             |
| bigint   | 8 bytes  | -9223372036854775808 to 9223372036854775807           |
| decimal  | variable | 131072 to 16383 digits before and after decimal point |
| numeric  | variable | 131072 to 16383 digits before and after decimal point |
| real     | 4 bytes  | 6 decimal digits precision                            |
| double   | 8 bytes  | 15 decimal digits precision                           |


## Date/Time Datatypes

PostgreSQL timestamp supports down to microsecond precision with or without
timezone. Both dates and times are supported in a variety of formats with
Day / Month / Year ordering options given by DMY, MDY, and YMD.

[ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) is an international
standard for dates and times with a presentation principle of ordering units
from largest to smallest (YMD).

| Name                       | Size     | Range                               |
|----------------------------|----------|-------------------------------------|
| Timestamp without timezone | 8 bytes  | 4713 BC to 294276 AD to 1 microsec  | 
| Timestamp with timezone    | 8 bytes  | 4713 BC to 294276 AD to 1 microsec  | 
| date                       | 4 bytes  | 4713 BC to 294276 AD to 1 day       | 
| Time without timezone      | 8 bytes  | 00:00:00 to 24:00:00 to 1 microsec  | 
| Time with timezone         | 12 bytes | 00:00:00 + 1459 to 24:00:00 - 1459  | 
| Interval                   | 12 bytes | -178000000 to 178000000 years - 1ms | 

### Examples

| Input             |  Description                                            |
|-------------------|---------------------------------------------------------|
| 2025-09-07        | ISO 8601, September 7 with any date style (recommended) |
| September 7, 2025 | September 7, 2025 with any date style                   |
| 9/7/2025          | September 7, 2025 with MDY, July 9 with DMY             |
| 9/7/25            | September 7, 2025 with MDY                              |
| 2025-Sep-7        | September 7, 2025 with any date style                   |
| Sep-7-2025        | September 7, 2025 with any date style                   |
| 20250907          | ISO 8601, 7 Sept 2025 in any mode                       |
| 2025.250          | September 7, 2025 with year and day of year             |
| J25250            | Julian date                                             |
| 11:19:38.507      | ISO 8601 Time to millisecond                            |
| 11:19:38          | ISO 8601 Time to second                                 |
| 11:19             | ISO 8601 Time to hour                                   |
| 111938            | ISO 8601 Time to second                                 |
| 11:19 AM          | Same as 11:19                                           |
| 11:19 PM          | Same as 23:19                                           |
| 23:19-5           | ISO 8601, same as 11:19 PM EST                          |
| 23:19-05:00       | ISO 8601, same as 11:19 PM EST                          |
| 2319-05           | ISO 8601, same as 11:19 PM EST                          |
| 23:19 EST         | Time zone specified, same as 11:19 PM EST               |


## Boolean Datatype

A Boolean data type can hold `1`, `0`, or `null` values and is specified with
the keywords **bool** or **boolean**.

Postgres converts `Yes`, `y`, `t`, and `true` to `1` and `No`, `n`, `f`, and
`false` to `0` internally, but the values are converted back to their given
form when selected.
