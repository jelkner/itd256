# Create Personal Database on austin

The session to create a personal database on our in class austin server looks
like this:
```
aabela@austin:~$ sudo su - postgres
postgres@austin:~$ createuser aabela
postgres@austin:~$ createdb aabela
postgres@austin:~$ psql 
psql (13.8 (Debian 13.8-0+deb11u1))
Type "help" for help.

postgres=# ALTER USER aabela WITH ENCRYPTED PASSWORD 'password';
ALTER ROLE
postgres=# GRANT ALL PRIVILEGES ON DATABASE aabela TO aabela;
GRANT
postgres=# 
```
