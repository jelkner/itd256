# Getting Started with Databases

## Lesson 1: [Introduction to Databases](https://www.youtube.com/watch?v=4Z9KEBexzcM)

### Topics

* The problems with lists.
* Why use a database?
* How relational tables solve the problems of lists.

### Purpose of a Database

To:

* store data
* provide an organizational structure for data
* provide a mechanism for querying, creating, modifying, and deleting data,
  (often referred to as **CRUD** - create, read, update, delete)

The first lesson motivates the use of databases by describing examples of the
problems with lists. Specifically:

* deletion anomalies
* update anomalies
* insertion anomalies
