# Setting Up PostgreSQL Server on Debian 11

## Install and Configure

Used [Vote Arlington 1: Setting Up the Postgis Database](http://www.openbookproject.net/tutorials/explore_fsgis/vote_arlington1.html) for guidance.

1. Ran:
   ```
   $ sudo apt install postgis
   ```
   to install Postgres 13.

2. Edit ``/etc/postgresql/13/main/pg_hba.conf`` changing:
   ```
   host  all		all	127.0.0.1/32	md5
   ```
   to:
   ```
   host  all		all	0.0.0.0/0	md5
   ```

3. Edit ``/etc/postgresql/13/main/postgresql.conf`` changing:
   ```
   #listen_addresses = 'localhost'
   ```
   to:
   ```
   listen_addresses = '*'
   ```

4. Give user database superuser privileges and set its postgress password with: 
   ```
   $ sudo su - postgres
   $ createuser --superuser [user]
   $ psql -c "ALTER ROLE [user] PASSWORD '[password]'"
   ```

5. Create a database with:
   ```
   $ createdb curios_moon
   ```

## Add Users with DB Access

Used [Creating user, database and adding access on PostgreSQL](https://medium.com/coding-blocks/creating-user-database-and-adding-access-on-postgresql-8bfcd2f4a91e) for guidance.
